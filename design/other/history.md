# 历史重要更新（贡献记录）
（除了小修改外都算重要更新，包括改动小，但影响大的）

```
---------------------
20211225 - Messy
---------------------
add kicad project
add .gitignore
add .gitattributes
更新内容（./design/KiCad/）
what is .gitignore and .gitattributes?
look here: 
about .gitattributes
https://www.jianshu.com/p/450cd21b36a4
https://blog.csdn.net/lihuaichen/article/details/80989147
about .gitignore
https://zhuanlan.zhihu.com/p/341234283
https://github.com/github/gitignore


---------------------
20211223 - Messy
---------------------
新增第一次打板总结
更新内容（./design/design.md）


---------------------
20211221 - Messy
---------------------
新增usb转串口模块CH340原理图和PCB
更新内容（./design/design.md & ./design/CH340PCB/）


---------------------
20211219 - Messy
---------------------
最小应用系统已经设计完成，开始设计usb转串口模块
更新内容（./design/design.md）


---------------------
20211216 - Messy
---------------------
原理探索基本完成，开始进行设计电路板


---------------------
20211215 - Messy
---------------------
立创eda（专业版）创建团队，都注册一个，该项目使用立创EDA来画pcb，
在线编辑可以一起画，方便查看和共享


---------------------
20211211 - Messy
---------------------
新增了STC89C52系列芯片手册(STC89C52.pdf)，手册中包含了最小系统的原理图，
以及最小系统开发板的参考原理图，同时包含了单片机的功能介绍。
```