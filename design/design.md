# STC51单片机最小系统开发板的设计与制作



## 一、大体结构与芯片选型

### 大体结构

首先是实现最小应用系统（手册21页），然后是实现程序下载电路（即USB转串口模块）

### 主芯片选型

从手册中了解到STC89C52系列单片机有多种封装，而我们协会有一堆`PDIP-40`封装的`STC89C52RC`，同时从焊接与调试的角度出发，选择该芯片用作本项目使用。

实物图：

![ecf379056a937dba2bdc25cc2c6efa0](design.assets/ecf379056a937dba2bdc25cc2c6efa0-16395787539451.jpg)

### USB转串口芯片

打算使用CH340

理由: 便宜,常用,简单

### 二、最小应用系统设计

图：手册21页

![image-20211219164037532](design.assets/image-20211219164037532.png)



eda项目地址：

https://pro.lceda.cn/editor#id=dd9413feb80740aebcd815b7715a45ad

#### 第一次打板经验总结
1. io口要有端口号丝印，否则不方便接线
2. 板子应该有LED指示灯，方便调试
3. 在宽裕的空间内布线应该宽且分散，减少损耗和干扰


## 三、USB转串口模块设计

**什么是串口？**

[3分钟理解通信协议之串口UART到底是个啥？](https://www.bilibili.com/video/BV1cQ4y1C7hj)

[【硬件干货】串口到底是个啥？](https://www.bilibili.com/video/BV1ZC4y1a7pG)

[【讲的太透了！嵌入式经典总线协议】UART、RS232、RS485、IIC、SPI串口协议精讲](https://www.bilibili.com/video/BV1WK4y1o7rd)



### CH340

[超简单的USB转串口方案，支持国产！](https://www.bilibili.com/video/BV19y4y1G7ke)

[快速上手CH340G USB转串口芯片，并了解CH340系列。在面包板搭建一个单片机下载器【IC原来如此】](https://www.bilibili.com/video/BV1hK4y1A7sr)

沁恒官网：http://www.wch.cn/

沁恒官网的CH340资料：http://www.wch.cn/products/CH340.html?from=list

官网有参考原理图和PCB（已经上传到本仓库[CH340PCB](./CH340PCB/)）

So。。。。。 it's easy

例图：

CH340G9T-R0

![image-20211221131855462](design.assets/image-20211221131855462.png)



![image-20211221131829893](design.assets/image-20211221131829893.png)
